import { createFighters } from './components/fightersView';
import { createFighterPreview } from './components/fighterPreview';
import { fighterService } from './services/fightersService';
import { fightersSelector } from './components/fighterSelector';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';

      const fighters = await fighterService.getFighters();
      // const fightersInfo = await fighterService.getFighterDetails(1);
      // console.log(fightersInfo)
      // console.log(fighters)


      const fightersElement = createFighters(fighters);
      // console.log(fightersElement)

      // const getFightersInfo = createFighterPreview(fightersInfo);

      App.rootElement.appendChild(fightersElement);
      // App.rootElement.appendChild(getFightersInfo);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;
