import { createElement } from '../helpers/domHelper';
import { fighters } from '../helpers/mockData';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if(fighter !== undefined){
    // console.log(fighter.name, position)
    const fighterImg = createFighterImage(fighter)
    const fighterName = createElement({tagName : 'span', className: 'fighter-name'})
    const fighterInfo = createElement({tagName: 'div', className: 'fighter-info'})
    const fighterInfoWrap = createElement({tagName: 'div', className: 'wrap-fighter-info'})

    fighterInfo.innerHTML = `
      <div class="fighter-info-item"> 
        <div>Health: ${fighter.health}</div>
      </div>
      <div class="fighter-info-item"> 
        <div>Attack: ${fighter.attack} </div>
      </div>
      <div class="fighter-info-item"> 
        <div>Defence: ${fighter.defense} </div>
      </div>
    `
    fighterName.innerText = fighter.name
    fighterInfoWrap.append(fighterName, fighterInfo)
    fighterElement.append(fighterImg, fighterInfoWrap)

  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  console.log(fighter)
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
