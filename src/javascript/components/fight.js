import { controls } from '../../constants/controls';
import { getRandomFloatFromRange } from '../helpers/getRandomFloatFromRange';


export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  if(damage > 0) return damage
  else return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandomFloatFromRange(1, 2);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandomFloatFromRange(1, 2);
  return fighter.defense * dodgeChance;
}
